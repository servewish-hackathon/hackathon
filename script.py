import time
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import bs4 
from urllib.request import Request, urlopen
from random import randint
from selenium.common.exceptions import NoSuchElementException

import sys
print("Loading...")
exec = True

print("Ready>>>")
chrome_path = r"E:\Servewish\data\chromedriver.exe"
browser = webdriver.Chrome(chrome_path)

while(exec):
    try:
        for key_search in range(0, len(var_list)):
            var_search_term = var_list[key_search]
            # browser.get('http://www.google.com')
            # search = browser.find_element_by_name('q')
            # search.send_keys(var_search_term)
            # search.send_keys(Keys.RETURN)
            # translate
            try:
                browser.find_element_by_xpath("""//*[@id="Rzn5id"]/div/a[2]""").click()
            except:
                pass
            # auto search inside restaurant mode
                browser.get("https://www.google.com/search?tbm=lcl&ei=QKYLXe_7EIigvQTt5rR4&q=+&oq=+&gs_l=psy-ab.3...3242.7512.0.8246.0.0.0.0.0.0.0.0..0.0....0...1c.1.64.psy-ab..0.0.0....0.VtBU2Xn2mz0#rlfi=hd:;si:;mv:!1m2!1d11.6530157!2d104.9822489!2m2!1d10.3102849!2d104.00503599999999!3m12!1m3!1d651487.3504876399!2d104.49364245000001!3d10.981650299999998!2m3!1f0!2f0!3f0!3m2!1i356!2i499!4f13.1;tbs:lrf:!2m1!1e2!2m1!1e3!2m4!1e17!4m2!17m1!1e2!3sIAE,lf:1,lf_ui:2")
                search = browser.find_element_by_name('q')
                search.send_keys(var_search_term)
                search.send_keys(Keys.RETURN)
                html_source = browser.page_source
                page_soup = BeautifulSoup(html_source, "html.parser")
                # per page
                try:
                    containers = page_soup.findAll("a", {"class":"fl"})
                except:
                    pass
                # print("PROCESSING...")
                try:
                    for i in range(2, 50):
                        var_ran = randint(6, 8)
                        var_ran_small = randint(2, 4)
                        print(">>Keyword: ", var_search_term)
                        # print(">>COUNTING LIST...")
                        # time.sleep(var_ran)
                        # time.sleep(2)
                        html_source = browser.page_source
                        page_soup = BeautifulSoup(html_source, "html.parser")
                        list_containers = page_soup.findAll("div", {"class":"dbg0pd"})
                        print("Number of List: ",len(list_containers))
                        print(">>Index Page",i-1) 
                        # loop through list
                        for j in range(1, len(list_containers)+1):
                            # time.wait(1)
                            print("List:",j)
                            print("content loading...")
                            time.sleep(1)
                            # some use 4 some use 5 so it is not accessible across the web
                            for xpath in range(1, 9):
                                # print("XPATH: ", xpath)
                                
                                try:
                                    
                                    xpath_restaurant = browser.find_element_by_xpath("//*[@id='rl_ist0']/div[1]/div["+str(xpath)+"]/div["+str(j)+"]")
                                    xpath_restaurant.click()
                                    break
                                except:
                                    pass
                            time.sleep(var_ran_small)  
                            html_source = browser.page_source
                            page_soup = BeautifulSoup(html_source, "html.parser")
                            whole_card_details = page_soup.findAll("div", {"class":"ifM9O"})
                            try:
                                title_container = whole_card_details[0].findAll("div",{"class":"SPZz6b"})
                                var_title = title_container[0].div.span.text
                                print("Title: ",var_title)
                            except:
                                pass
                            try:
                                link_container = whole_card_details[0].findAll("div",{"class":"QqG1Sd"})
                                var_link = link_container[0].a["href"]
                                print("URL: ", var_link)
                            except:
                                pass
                            try:
                                # rtng_container = card_details[j].findAll("span",{"class":"rtng"})
                                rtng_container = whole_card_details[0].findAll("span",{"class":"rtng"})
                                var_rtng = rtng_container[0].text
                                print("Rating: ", var_rtng)
                            except:
                                pass
                            try:
                                review_container = whole_card_details[0].findAll("span",{"class":"fl"})
                                var_review = review_container[0].span.a.span.text     
                                print("Review: ", var_review)
                            except:
                                pass
                            try:
                                adrs_container = whole_card_details[0].findAll("span",{"class":"LrzXr"})
                                var_adrs = adrs_container[0].text   
                                print("Address: ", var_adrs)
                            except:
                                pass 
                            try:
                                hour_container = whole_card_details[0].findAll("span",{"class":"TLou0b"})
                                var_hour = hour_container[0].span.text       
                                print("Hour: ", var_hour)
                            except:
                                pass 
                            try:
                                phone_container = whole_card_details[0].findAll("span",{"class":"LrzXr zdqRlf kno-fv"})
                                var_phone = phone_container[0].span.span.text         
                                print("Phone: ", var_phone)
                            except:
                                pass

                        browser.find_element_by_css_selector("a[aria-label='Page "+str(i)+"'][class='fl']").click()
                        var_search_url = browser.current_url
                        print("=================================")
                except:
                    pass
    except:
        pass
    finally:
        browser.switch_to.default_content()   
    var_terminate = input("Do you want to re-execute the program?[Y]/[N]")
    if var_terminate == "y" or var_terminate =="Y":
        print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Program is running...")
        quit
    else:
        sys.exit(0)
